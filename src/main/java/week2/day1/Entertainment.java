package week2.day1;

public interface Entertainment {
	
	public void play();
	
	public void change();
	
	public void volume();
	
	
}
