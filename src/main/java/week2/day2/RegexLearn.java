package week2.day2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexLearn {
	public static void main(String[] args) {
		String txt = "Deepika.Jayachandran@Cognizant.com";
		String pattern ="[A-Z]{1}[a-z]{6}[.][A-Z]{1}[a-z]{11}[@][A-Z]{1}[a-z]{8}[.][a-z]{3}";
		Pattern compile =Pattern.compile(pattern);
		Matcher matcher =compile.matcher(txt);
		System.out.println(matcher.matches());
	}

}
