package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://www.crystalcruises.com/cruises/calendar?year=2018");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        List<WebElement> eleFindElements = driver.findElementsByXPath("//a[text()='Request A Quote']");
        System.out.println(eleFindElements.size());
        
        int count = eleFindElements.size();
        for (WebElement each : eleFindElements) {
			System.out.println(each.getText());
		}
        eleFindElements.get(count-1).click();
	}

}
