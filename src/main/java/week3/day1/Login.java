package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://leaftaps.com/opentaps/control/main");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElementById("username").sendKeys("DemoSalesManager");
        driver.findElementById("password").sendKeys("crmsfa");
        driver.findElementByClassName("decorativeSubmit").click();
        driver.findElementByLinkText("CRM/SFA").click();
        Thread.sleep(5000); 
        driver.findElementByLinkText("Create Lead").click();
        driver.findElementById("createLeadForm_company	Name").sendKeys("TestLeaf");
        driver.findElementById("createLeadForm_firstName").sendKeys("Deepika");
        driver.findElementById("createLeadForm_lastName").sendKeys("Jaya");
       
        WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(eleSource));
        Select sel = new Select(eleSource);
       // sel.selectByVisibleText("Public Relations");
       // sel.selectByValue("LEAD_EXISTCUST");
       // sel.selectByIndex(6);
        List<WebElement> allOptions = sel.getOptions();
        int count = allOptions.size();
        System.out.println(count);
        for (WebElement eachOption : allOptions) {
			System.out.println(eachOption.getText());
		}
        sel.selectByIndex(count-1);
      //  driver.findElementByName("submitButton").click();
        
        
        
	}

}





