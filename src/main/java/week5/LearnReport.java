package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {
	public static void main(String[] args) throws IOException {
		
	ExtentHtmlReporter html =new ExtentHtmlReporter("./Reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);
	ExtentTest logger =extent.createTest("TC_001 Create Lead", "Create New Lead:");
	logger.assignAuthor("Deepika");
	logger.assignCategory("Regression");
	logger.log(Status.PASS, "The Data DemoSalesManager Entered sucessfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	logger.log(Status.PASS, "The Data crmsfa Entered sucessfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	logger.log(Status.PASS, "The Login Button  clicked sucessfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	logger.log(Status.PASS, "The Link CRM Clicked sucessfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	logger.log(Status.PASS, "The Create Lead Clicked sucessfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	
	extent.flush();
			

}
}