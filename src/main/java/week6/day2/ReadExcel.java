package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	//@Test
	public static Object[][] readExcel () throws IOException {
	XSSFWorkbook wb =new XSSFWorkbook("./data/createLead.xlsx");
	XSSFSheet sheet =wb.getSheetAt(0);    //go to sheet
	int rowCount = sheet.getLastRowNum(); //row count
	System.out.println(rowCount);
	int cellCount = sheet.getRow(0).getLastCellNum();//column count
	System.out.println(cellCount);
	Object[][] data =new Object[rowCount][cellCount];
	for (int j =1; j <=rowCount ; j++) {
		XSSFRow row = sheet.getRow(j);//go to specific row
		for (int i = 0; i < cellCount; i++) {
			XSSFCell cell = row.getCell(i);// go to specific row
			try {
				String value = cell.getStringCellValue();// go to the content (string )
				System.out.println(value);
				data[j-1][i] =value;
			} catch (NullPointerException e) {
				System.out.println();
			}
		}
		
	}
	
	wb.close();
	return data;
	}
}
