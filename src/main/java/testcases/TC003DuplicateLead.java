package testcases;

	import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC003DuplicateLead extends ProjectMethods{
	
	@BeforeTest(groups = {"any"})
	public void setData() {
		testCaseName = "TC003_DuplicateLead";
		testDesc = "DuplicateLead A Lead";
		author = "Deepika";
		category = "regression";
	}
	//(groups = {"regression"},dependsOnGroups = {"sanity"})
	@Test(groups = {"regression"})
	public void duplicateLead() {
		
	WebElement duplicate=locateElement("linktext","Duplicate Lead");
    duplicate.click();
    type(locateElement("createLeadForm_primaryPhoneNumber"), "9994109768");
	type(locateElement("createLeadForm_primaryPhoneAskForName"), "DeepikaJaya");
	WebElement create1=locateElement("xpath","//input[@class='smallSubmit']");
	create1.click();

	
	
	
}
}