package testcases;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week6.day2.ReadExcel;

public class TC001CreateLead extends ProjectMethods{
	
	@BeforeTest(groups = {"any"})
		public void setData() {
			testCaseName = "TC001_CreateLead";
			testDesc = "Create A new Lead";
			author = "Deepika";
			category = "smoke";
		}
	//invocationCount =2,timeOut =10000
	//enabled=false
	//groups = {"smoke"}
	//(dataProvider ="qa",indices={1})
	@Test(dataProvider ="qa")
	public void createLead(String cname,String fname,String lname,String ename,String ph) {
		
		WebElement cl = locateElement("linktext", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), cname);
		type(locateElement("createLeadForm_firstName"), fname);
		type(locateElement("createLeadForm_lastName"), lname);
		type(locateElement("createLeadForm_primaryEmail"), ename);
		type(locateElement("createLeadForm_primaryPhoneNumber"), ""+ph);
		click(locateElement("class", "smallSubmit"));
	}
	
@DataProvider(name="qa")	
public Object[][] fetchData() throws IOException{
	Object[][] data= ReadExcel.readExcel();
	return data;
/*	Object[][] data =new Object[2][5];
	data[0][0]="CTS";
	data[0][1]="Deepika";
	data[0][2]="Jaya";
	data[0][3]="Deepika.jayach@gmail.com";
	data[0][4]= 999410976;
	
	
	data[1][0]="Hexaware";
	data[1][1]="Deepa";
	data[1][2]="Jaya";
	data[1][3]="Deepa.jayach@gmail.com";
	data[1][4]= 999410976;
	return data;*/
}
			
}
	
	













