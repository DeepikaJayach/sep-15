package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC002EditLead extends ProjectMethods{

	@BeforeTest(groups = {"any"})
		public void setData() {
			testCaseName = "TC002_EditLead";
			testDesc = "Edit A Lead";
			author = "Deepika";
			category = "sanity";
		}
	//(dependsOnMethods= {"package.classname.Methods"},alwaysRun=true)
	//(dependsOnMethods= {"testcases.TC001CreateLead.createLead"},alwaysRun=true)
	//(dependsOnMethods= {"testcases.TC001CreateLead.createLead"})
	//(groups = {"sanity"},dependsOnGroups = {"smoke"})
	@Test(groups = {"sanity"})
	public void editLead() {
		
		WebElement cl = locateElement("linktext", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), "TL");
		type(locateElement("createLeadForm_firstName"), "Deepika");
		type(locateElement("createLeadForm_lastName"), "Jayach");
		click(locateElement("class", "smallSubmit"));
	}
	
}












