package projectDay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods {
	@Test
	public void bookZoomcar() {
	startApp("chrome","https://www.zoomcar.com/chennai");
	WebElement enter = locateElement("xpath","//a[@class = 'search']");
	click(enter);
	WebElement pup = locateElement("xpath","//div[contains(text(),'Thuraip')]");
	click(pup);
	WebElement nxt1 = locateElement("xpath","//button[@class = 'proceed']");
	click(nxt1);
	Date date = new Date();
	DateFormat sdf = new SimpleDateFormat("dd"); 
	String today = sdf.format(date);
	int tomorrow = Integer.parseInt(today)+1;
	System.out.println(tomorrow);
	WebElement date1 = locateElement("xpath","//div[text()="+tomorrow+"]");
	click(date1);
	WebElement nxt2 = locateElement("xpath","//button[@class = 'proceed']");
	click(nxt2);
	WebElement done = locateElement("xpath","//button[contains(text(),'Done')]");
	click(done);
	List<WebElement> prices = driver.findElementsByXPath("//div[@class = 'price']");
    System.out.println(prices.size());
/*    WebElement sort = locateElement("xpath","(//div[@class= 'item'])[7]");
	click(sort);
	WebElement brand = locateElement("xpath","(//div[])[1]");
	click(sort);*/
		
	   
    
   List<String> carprice=new ArrayList<>();
	for(WebElement abc:prices)
	{
		String text = abc.getText();
		//System.out.println(text);
		carprice.add(text.substring(2));
		}
	String q=Collections.max(carprice);
	System.out.println(q);
    
}
}