package week1.day1;

public class MyPhone {

	public static void main(String[] args) {
		//syntax to call another class
		//ClassName objectName = new ClassName();
		MobilePhone my = new MobilePhone();
		//syntax to call a method
		//ObjectName.MethodName(); or ObjectName.variable;
		my.dialCaller("Ravi");
		String imei = my.imei;
		System.out.println(imei);
		int age = my.age;
		System.out.println(age);

	}

}
