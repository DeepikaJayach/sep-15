package week1.day1;

public class SendSMS {

	public static void main(String[] args) {
		//syntax to call another class
				//ClassName objectName = new ClassName();
				MobilePhone1 my = new MobilePhone1();
				//syntax to call a method
				//ObjectName.MethodName(); or ObjectName.variable;
				my.("Ravi");
				String imei = my.imei;
				System.out.println(imei);
				int age = my.age;
				System.out.println(age);

			}

		}
