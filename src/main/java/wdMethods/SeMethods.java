package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;


public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions op =new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
				} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("Pass", "The Browser "+browser+" Launched Successfully");
			//System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public void reportStep(String string, String string2) {
		// TODO Auto-generated method stub
		
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			}			
			reportStep("Pass", "The Element "+locValue+" Is Located Successfully");
		} catch (NoSuchElementException e) {
			reportStep("fail", "The Element "+locValue+" Is Not Located ");
			//System.out.println("The Element Is Not Located ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			//System.out.println("The Data "+data+" is Entered Successfully");
			reportStep("pass", "The Data "+data+" is Entered Successfully");
		} catch (Exception e) {
			reportStep("fail", "The Data "+data+" Not Entered");
		}
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("pass", "The data: "+ele+" is clicked successfully");
			//System.out.println("The Element "+ele+" Clicked Successfully");
			takeSnap();
		} catch (WebDriverException e) {
			reportStep("fail", "The data: "+ele+" is not clicked");
		}
	}
	public void clickWitOutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			//System.out.println("The DropDown Is Selected with "+value);
			reportStep("pass", "The DropDown "+value+" is selected Successfully");
		} catch (Exception e) {
			reportStep("fail", "The DropDown "+value+" is Not selected");
			e.printStackTrace();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("pass", "The data: "+ele+" is clicked successfully");
			takeSnap();
		} catch (WebDriverException e) {
			reportStep("fail", "The data: "+ele+" is not clicked");
			takeSnap();
		}


	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title=driver.getTitle();
		if(title==expectedTitle)
		{
		System.out.println("entered title is verified");
		takeSnap();
		return true;
		}
		else
		{
			takeSnap();
			return false;
			
		}

	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text=ele.getText();
		if(text== expectedText)
		{
		System.out.println("text is verified");
		takeSnap();
		}
		else 
		{
			System.out.println("text is not verified");
			takeSnap();
		}
	


	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text=ele.getText();
		if(text.contains(expectedText))
		{
		System.out.println("text is partially verified");
		takeSnap();
		}
		else
		{
			System.out.println("text is not partially verified");
			takeSnap();
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String attribute1=ele.getAttribute(value);
		if( attribute1==attribute)
		{
		System.out.println("text is verified with attribute");
		takeSnap();
		}
		else
		{
			System.out.println("text is not verified with attribute");
			takeSnap();
		}


	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String attribute1=ele.getAttribute(value);
		if( attribute1.contains(attribute))
		{
		System.out.println("text is verified partially with attribute");
		takeSnap();
		}
		
		else
		{
			System.out.println("text is not verified with attribute");
			takeSnap();
		}


	}

	@Override
	public void verifySelected(WebElement ele) {
		if(ele.isSelected())
		{
		System.out.println("webelement is selected");
		takeSnap();
		}
		else
		{
			System.out.println("webelement is not selected");
			takeSnap();
		}


	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed())
		{
		System.out.println("webelement is Displayed");
		takeSnap();
		}
		else
		{
			System.out.println("webelement is not Displayed");
			takeSnap();
		}


	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> listOfWindow = new ArrayList<String>();
			listOfWindow.addAll(allWindows);
			driver.switchTo().window(listOfWindow.get(index));
			reportStep("pass", "switched to window");
			//System.out.println("The Window is Switched ");
			} catch (Exception e) {			
				reportStep("fail", "not switched to window");			
			e.printStackTrace();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			//System.out.println("switched to frame");
			reportStep("pass", "switched to frame");
			takeSnap();
		} catch (WebDriverException e) {
			reportStep("fail", "not switched to frame");
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String alerttext=driver.switchTo().alert().getText();
		takeSnap();
return alerttext;

	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
