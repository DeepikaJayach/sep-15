package week4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {
	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver","E:\\Testleaf\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.irctc.co.in/nget/train-search");
	    driver.findElementByLinkText("AGENT LOGIN").click();
	    driver.findElementByLinkText("Contact Us").click();
	    Set<String> allwindow1 = driver.getWindowHandles();
	    List<String> lst1 = new ArrayList<>();
	    lst1.addAll(allwindow1);
	    driver.switchTo().window(lst1.get(1));
	    System.out.println(driver.getTitle());
	    File src =driver.getScreenshotAs(OutputType.FILE);
	   File obj =new File ("./Snaps/Imag1.Jpeg");
	   FileUtils.copyFile(src,obj);
		}


}
