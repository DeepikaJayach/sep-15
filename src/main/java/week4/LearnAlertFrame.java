package week4;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {
	
	public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver","E:\\Testleaf\\chromedriver_win32\\chromedriver.exe");
	ChromeDriver driver= new ChromeDriver();
    driver.manage().window().maximize();
    driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
    Thread.sleep(3000);
    driver.switchTo().frame(1);
    driver.findElementByXPath("//button[text()='Try it']").click();
    driver.switchTo().alert().getText();
    driver.switchTo().alert().sendKeys("Deepika");
    driver.switchTo().alert().accept();
    driver.findElementById("demo").getText();
    
    
       
	}

}
